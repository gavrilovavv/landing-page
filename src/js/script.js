(function () {
    "use strict";

    let carouselDots = document.getElementById('carousel-dots');
    let reviewsCarousel = document.querySelector('#reviews-carousel');

    reviewsCarousel.addEventListener('slid.bs.carousel', function (event) {
        carouselDots.querySelector(`[data-bs-slide-to="${event.from}"]`).classList.remove('active');
        carouselDots.querySelector(`[data-bs-slide-to="${event.to}"]`).classList.add('active');
    });
})();
